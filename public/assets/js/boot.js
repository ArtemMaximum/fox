(function (require, requirejs, window) {
    'use strict';

    requirejs.config({
        baseUrl: './',
        waitSeconds: 200,
        urlArgs: 'v=' + parseInt((new Date()).getTime() / 86400000),

        paths: {
            /**
             * Requirejs plugins
             */

            jquery: 'vendor/jquery/dist/jquery.min',
            respond: 'vendor/respond/dest/respond.min',

            /**
             * UI libs
             */

            bootstrap: 'vendor/bootstrap/dist/js/bootstrap.min',

            /** App Modules */

            app: 'src/app'
        },
        shim: {
            jquery: {
                exports: 'jquery'
            },
            respond: {
                deps: ['jquery']
            },
            bootstrap: {
                deps: ['jquery']
            }
        }
    });

    if (Boolean(window.LOAD_MODULE)) {
        require(['app']);
    }

})(require, requirejs, window);